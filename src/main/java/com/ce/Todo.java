package com.ce;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.cloud.gcp.data.datastore.core.mapping.Entity;
import org.springframework.data.annotation.Id;

@Entity
public class Todo {

	@Id
	Long id;

	@NotNull(message = "User cannot be null!")
	@NotBlank(message = "User cannot be blank!")
	private String user;

	@Size(min = 5, max = 255, message = "Description has to be between 5 and 255 characters")
	private String description;

	private String targetDate;

	private boolean done;

	public Long getId() {
		return id;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public boolean isDone() {
		return done;
	}

	public void setDone(boolean done) {
		this.done = done;
	}

	public Todo(String user, String description, String targetDate, boolean done) {
		super();
		this.user = user;
		this.description = description;
		this.targetDate = targetDate;
		this.done = done;
	}

	public String getTargetDate() {
		return targetDate;
	}

	public void setTargetDate(String targetDate) {
		this.targetDate = targetDate;
	}

	public Todo() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		return "Todo [id=" + id + ", user=" + user + ", description=" + description + ", targetDate=" + targetDate
				+ ", done=" + done + "]";
	}

}

